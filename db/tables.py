from sqlalchemy import Column, Integer, Float, Date, String, VARCHAR, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey

import os

db_path = os.path.abspath('db/rsms.db')
db_url = 'sqlite:///' + db_path

Base = declarative_base()

class Marks(Base):
	"""docstring for Marks"""
	__tablename__ = 'marks'
	regno = Column(VARCHAR, primary_key=True, nullable=False)
	subject = Column(VARCHAR, primary_key=True, nullable=False)
	semester = Column(VARCHAR)
	int_marks = Column(Integer)
	ext_marks = Column(Integer)
	marks = Column(Integer)
	status = Column(VARCHAR)
	grade = Column(VARCHAR)

class Student(Base):
	"""Abstract class for student"""
	__tablename__ = 'student'
	# __table_args__ = {'sqlite_autoincrement': True}
	regno = Column(VARCHAR, ForeignKey('marks.regno'), primary_key=True, nullable=False)
	name = Column(VARCHAR)
	branch = Column(VARCHAR)
	# semester = Column(VARCHAR)
	# __mapper_args__ = {
	# 	'polymorphic_identity': 'student',
	# 	'polymorphic_on': semester
	# }

class S1S2(Base):
	"""Class for S1S2 table"""
	__tablename__ = 's1s2'
	regno = Column(VARCHAR, ForeignKey('student.regno'), primary_key=True, nullable=False)
	subject1 = Column(VARCHAR)
	subject2 = Column(VARCHAR)
	subject3 = Column(VARCHAR)
	subject4 = Column(VARCHAR)
	subject5 = Column(VARCHAR)
	subject6 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject8 = Column(VARCHAR)
	subject9 = Column(VARCHAR)
	subject10 = Column(VARCHAR)
	subject11 = Column(VARCHAR)
	total = Column(Integer)
	status = Column(VARCHAR)

class S3(Base):
	"""Class for S3 table"""
	__tablename__ = 's3'
	regno = Column(VARCHAR, ForeignKey('student.regno'), primary_key=True, nullable=False)
	subject1 = Column(VARCHAR)
	subject2 = Column(VARCHAR)
	subject3 = Column(VARCHAR)
	subject4 = Column(VARCHAR)
	subject5 = Column(VARCHAR)
	subject6 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject8 = Column(VARCHAR)
	total = Column(Integer)
	status = Column(VARCHAR)

class S4(Base):
	"""Class for S4 table"""
	__tablename__ = 's4'
	regno = Column(VARCHAR, ForeignKey('student.regno'), primary_key=True, nullable=False)
	subject1 = Column(VARCHAR)
	subject2 = Column(VARCHAR)
	subject3 = Column(VARCHAR)
	subject4 = Column(VARCHAR)
	subject5 = Column(VARCHAR)
	subject6 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject8 = Column(VARCHAR)
	total = Column(Integer)
	status = Column(VARCHAR)

class S5(Base):
	"""Class for S5 table"""
	__tablename__ = 's5'
	regno = Column(VARCHAR, ForeignKey('student.regno'), primary_key=True, nullable=False)
	subject1 = Column(VARCHAR)
	subject2 = Column(VARCHAR)
	subject3 = Column(VARCHAR)
	subject4 = Column(VARCHAR)
	subject5 = Column(VARCHAR)
	subject6 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject8 = Column(VARCHAR)
	total = Column(Integer)
	status = Column(VARCHAR)

class S6(Base):
	"""Class for S6 table"""
	__tablename__ = 's6'
	regno = Column(VARCHAR, ForeignKey('student.regno'), primary_key=True, nullable=False)
	subject1 = Column(VARCHAR)
	subject2 = Column(VARCHAR)
	subject3 = Column(VARCHAR)
	subject4 = Column(VARCHAR)
	subject5 = Column(VARCHAR)
	subject6 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject7 = Column(VARCHAR)
	subject8 = Column(VARCHAR)
	total = Column(Integer)
	status = Column(VARCHAR)

#######################################################
################ Implementation tables ################
#######################################################

class OperationMap(Base):
	__tablename__ = 'operation_map'
	phrase = Column(VARCHAR, primary_key=True)
	operation = Column(VARCHAR, primary_key=True)
	aggregate = Column(Boolean)

class RelationshipMap(Base):
	__tablename__ = 'relationship_map'
	phrase = Column(VARCHAR, primary_key=True)
	relationship = Column(VARCHAR, primary_key=True)

class FieldMap(Base):
	__tablename__ = 'field_map'
	phrase = Column(VARCHAR, primary_key=True)
	field = Column(VARCHAR, primary_key=True)
	field_type = Column(VARCHAR)

class FieldTableMap(Base):
	__tablename__ = 'field_table_map'
	field = Column(VARCHAR, primary_key=True)
	tables = Column(VARCHAR, primary_key=True)