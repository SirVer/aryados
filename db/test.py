from sqlalchemy import Column, Integer, Float, Date, String, VARCHAR
from sqlalchemy import ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import inspect

from tables import db_url, Base, Marks, Student, S1S2, S3, S4, S5, S6, OperationMap, RelationshipMap, FieldMap

engine = create_engine(db_url)
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

def merge_dicts(dicts):
	new_dict = {}
	for d in dicts:
		new_dict.update(d)
	return new_dict

filters = []
filters.append(Marks.subject.like('%theory of computation%'))
filters.append(Marks.marks > 70)

tables = [Student]
# for row_tuple in session.query(Student, Marks).join(Marks, S4).filter(*filters):
query = session.query(*tables).filter(Marks.regno == Student.regno).filter(*filters)
print(query)
i = 1
if len(tables) == 1:
	for row in query:
		row = inspect(row).dict
		del row['_sa_instance_state']
		print(i, row)
		i+=1
else:
	for row_tuple in query:
		super_dict = merge_dicts([inspect(row).dict for row in row_tuple])
		del super_dict['_sa_instance_state']
		print(i, super_dict)
		i+=1