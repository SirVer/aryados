from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import csv
import pandas as pd

from tables import db_url, Base, Marks, Student, S1S2, S3, S4, S5, S6

engine = create_engine(db_url)
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()
df_s1s2 = pd.read_csv('db/201510S1S2RegularMarklist.csv')
df_s3 = pd.read_csv('db/201605MayS3RegularMarklist.csv')
df_s4 = pd.read_csv('db/201612S4RegularMarklist.csv')
df_s5 = pd.read_csv('db/201706S5RegularMarklist.csv')
df_s6 = pd.read_csv('db/201709SepS6RegularMarklist.csv')

no_of_records = 625

for index, row in df_s1s2.iterrows():
	if row.A_Int == '-':
		row.A_Int = 0
	if row.A_Ext == '-':
		row.A_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.A_Skip, semester='s1s2', int_marks=row.A_Int, ext_marks=row.A_Ext, marks=int(row.A_Int)+int(row.A_Ext), status=row.A_Status, grade=row.A_Grade)
	session.merge(new_marks_row)
	if row.B_Int == '-':
		row.B_Int = 0
	if row.B_Ext == '-':
		row.B_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.B_Skip, semester='s1s2', int_marks=row.B_Int, ext_marks=row.B_Ext, marks=int(row.B_Int)+int(row.B_Ext), status=row.B_Status, grade=row.B_Grade)
	session.merge(new_marks_row)
	if row.C_Int == '-':
		row.C_Int = 0
	if row.C_Ext == '-':
		row.C_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.C_Skip, semester='s1s2', int_marks=row.C_Int, ext_marks=row.C_Ext, marks=int(row.C_Int)+int(row.C_Ext), status=row.C_Status, grade=row.C_Grade)
	session.merge(new_marks_row)
	if row.D_Int == '-':
		row.D_Int = 0
	if row.D_Ext == '-':
		row.D_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.D_Skip, semester='s1s2', int_marks=row.D_Int, ext_marks=row.D_Ext, marks=int(row.D_Int)+int(row.D_Ext), status=row.D_Status, grade=row.D_Grade)
	session.merge(new_marks_row)
	if row.E_Int == '-':
		row.E_Int = 0
	if row.E_Ext == '-':
		row.E_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.E_Skip, semester='s1s2', int_marks=row.E_Int, ext_marks=row.E_Ext, marks=int(row.E_Int)+int(row.E_Ext), status=row.E_Status, grade=row.E_Grade)
	session.merge(new_marks_row)
	if row.F_Int == '-':
		row.F_Int = 0
	if row.F_Ext == '-':
		row.F_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.F_Skip, semester='s1s2', int_marks=row.F_Int, ext_marks=row.F_Ext, marks=int(row.F_Int)+int(row.F_Ext), status=row.F_Status, grade=row.F_Grade)
	session.merge(new_marks_row)
	if row.G_Int == '-':
		row.G_Int = 0
	if row.G_Ext == '-':
		row.G_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.G_Skip, semester='s1s2', int_marks=row.G_Int, ext_marks=row.G_Ext, marks=int(row.G_Int)+int(row.G_Ext), status=row.G_Status, grade=row.G_Grade)
	session.merge(new_marks_row)
	if row.H_Int == '-':
		row.H_Int = 0
	if row.H_Ext == '-':
		row.H_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.H_Skip, semester='s1s2', int_marks=row.H_Int, ext_marks=row.H_Ext, marks=int(row.H_Int)+int(row.H_Ext), status=row.H_Status, grade=row.H_Grade)
	session.merge(new_marks_row)
	if row.J_Int == '-':
		row.J_Int = 0
	if row.J_Ext == '-':
		row.J_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.J_Skip, semester='s1s2', int_marks=row.J_Int, ext_marks=row.J_Ext, marks=int(row.J_Int)+int(row.J_Ext), status=row.J_Status, grade=row.J_Grade)
	session.merge(new_marks_row)
	if row.K_Int == '-':
		row.K_Int = 0
	if row.K_Ext == '-':
		row.K_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.K_Skip, semester='s1s2', int_marks=row.K_Int, ext_marks=row.K_Ext, marks=int(row.K_Int)+int(row.K_Ext), status=row.K_Status, grade=row.K_Grade)
	session.merge(new_marks_row)
	if row.L_Int == '-':
		row.L_Int = 0
	if row.L_Ext == '-':
		row.L_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.L_Skip, semester='s1s2', int_marks=row.L_Int, ext_marks=row.L_Ext, marks=int(row.L_Int)+int(row.L_Ext), status=row.L_Status, grade=row.L_Grade)
	session.merge(new_marks_row)
	new_student_row = Student(regno=row.RegNo, name=row.Name, branch=row.Branch,)
	session.merge(new_student_row)
	if row.SemTot == '-':
		row.SemTot = 0
	new_s1s2_row = S1S2(regno=row.RegNo,
						# name=row.Name,
						# branch=row.Branch,
						subject1=row.A_Skip,
						subject2=row.B_Skip,
						subject3=row.C_Skip,
						subject4=row.D_Skip,
						subject5=row.E_Skip,
						subject6=row.F_Skip,
						subject7=row.G_Skip,
						subject8=row.H_Skip,
						subject9=row.J_Skip,
						subject10=row.K_Skip,
						subject11=row.L_Skip,
						total=row.SemTot,
						status=row.SemStatus)
	session.merge(new_s1s2_row)
	session.commit()
	print('Importing S1S2... {}%'.format(int((index/no_of_records)*100)))
print('Importing S1S2... Done.')

for index, row in df_s3.iterrows():
	if row.A_Int == '-':
		row.A_Int = 0
	if row.A_Ext == '-':
		row.A_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.A_Skip, semester='s3', int_marks=row.A_Int, ext_marks=row.A_Ext, marks=int(row.A_Int)+int(row.A_Ext), status=row.A_Status, grade=row.A_Grade)
	session.merge(new_marks_row)
	if row.B_Int == '-':
		row.B_Int = 0
	if row.B_Ext == '-':
		row.B_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.B_Skip, semester='s3', int_marks=row.B_Int, ext_marks=row.B_Ext, marks=int(row.B_Int)+int(row.B_Ext), status=row.B_Status, grade=row.B_Grade)
	session.merge(new_marks_row)
	if row.C_Int == '-':
		row.C_Int = 0
	if row.C_Ext == '-':
		row.C_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.C_Skip, semester='s3', int_marks=row.C_Int, ext_marks=row.C_Ext, marks=int(row.C_Int)+int(row.C_Ext), status=row.C_Status, grade=row.C_Grade)
	session.merge(new_marks_row)
	if row.D_Int == '-':
		row.D_Int = 0
	if row.D_Ext == '-':
		row.D_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.D_Skip, semester='s3', int_marks=row.D_Int, ext_marks=row.D_Ext, marks=int(row.D_Int)+int(row.D_Ext), status=row.D_Status, grade=row.D_Grade)
	session.merge(new_marks_row)
	if row.E_Int == '-':
		row.E_Int = 0
	if row.E_Ext == '-':
		row.E_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.E_Skip, semester='s3', int_marks=row.E_Int, ext_marks=row.E_Ext, marks=int(row.E_Int)+int(row.E_Ext), status=row.E_Status, grade=row.E_Grade)
	session.merge(new_marks_row)
	if row.F_Int == '-':
		row.F_Int = 0
	if row.F_Ext == '-':
		row.F_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.F_Skip, semester='s3', int_marks=row.F_Int, ext_marks=row.F_Ext, marks=int(row.F_Int)+int(row.F_Ext), status=row.F_Status, grade=row.F_Grade)
	session.merge(new_marks_row)
	if row.G_Int == '-':
		row.G_Int = 0
	if row.G_Ext == '-':
		row.G_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.G_Skip, semester='s3', int_marks=row.G_Int, ext_marks=row.G_Ext, marks=int(row.G_Int)+int(row.G_Ext), status=row.G_Status, grade=row.G_Grade)
	session.merge(new_marks_row)
	if row.H_Int == '-':
		row.H_Int = 0
	if row.H_Ext == '-':
		row.H_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.H_Skip, semester='s3', int_marks=row.H_Int, ext_marks=row.H_Ext, marks=int(row.H_Int)+int(row.H_Ext), status=row.H_Status, grade=row.H_Grade)
	session.merge(new_marks_row)
	if row.SemTotal == '-':
		row.SemTotal = 0
	new_s3_row = S3(regno=row.RegNo,
					# name=row.Name,
					# branch=row.Branch,
					subject1=row.A_Skip,
					subject2=row.B_Skip,
					subject3=row.C_Skip,
					subject4=row.D_Skip,
					subject5=row.E_Skip,
					subject6=row.F_Skip,
					subject7=row.G_Skip,
					subject8=row.H_Skip,
					total=row.SemTotal,
					status=row.SemStatus)
	# new_s3_row.regno = row.RegNo
	session.merge(new_s3_row)
	session.commit()
	print('Importing S3... {}%'.format(int((index/no_of_records)*100)))
print('Importing S3... Done.')

for index, row in df_s4.iterrows():
	if row.A_Int == '-':
		row.A_Int = 0
	if row.A_Ext == '-':
		row.A_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.A_Skip, semester='s4', int_marks=row.A_Int, ext_marks=row.A_Ext, marks=int(row.A_Int)+int(row.A_Ext), status=row.A_Status, grade=row.A_Grade)
	session.merge(new_marks_row)
	if row.B_Int == '-':
		row.B_Int = 0
	if row.B_Ext == '-':
		row.B_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.B_Skip, semester='s4', int_marks=row.B_Int, ext_marks=row.B_Ext, marks=int(row.B_Int)+int(row.B_Ext), status=row.B_Status, grade=row.B_Grade)
	session.merge(new_marks_row)
	if row.C_Int == '-':
		row.C_Int = 0
	if row.C_Ext == '-':
		row.C_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.C_Skip, semester='s4', int_marks=row.C_Int, ext_marks=row.C_Ext, marks=int(row.C_Int)+int(row.C_Ext), status=row.C_Status, grade=row.C_Grade)
	session.merge(new_marks_row)
	if row.D_Int == '-':
		row.D_Int = 0
	if row.D_Ext == '-':
		row.D_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.D_Skip, semester='s4', int_marks=row.D_Int, ext_marks=row.D_Ext, marks=int(row.D_Int)+int(row.D_Ext), status=row.D_Status, grade=row.D_Grade)
	session.merge(new_marks_row)
	if row.E_Int == '-':
		row.E_Int = 0
	if row.E_Ext == '-':
		row.E_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.E_Skip, semester='s4', int_marks=row.E_Int, ext_marks=row.E_Ext, marks=int(row.E_Int)+int(row.E_Ext), status=row.E_Status, grade=row.E_Grade)
	session.merge(new_marks_row)
	if row.F_Int == '-':
		row.F_Int = 0
	if row.F_Ext == '-':
		row.F_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.F_Skip, semester='s4', int_marks=row.F_Int, ext_marks=row.F_Ext, marks=int(row.F_Int)+int(row.F_Ext), status=row.F_Status, grade=row.F_Grade)
	session.merge(new_marks_row)
	if row.G_Int == '-':
		row.G_Int = 0
	if row.G_Ext == '-':
		row.G_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.G_Skip, semester='s4', int_marks=row.G_Int, ext_marks=row.G_Ext, marks=int(row.G_Int)+int(row.G_Ext), status=row.G_Status, grade=row.G_Grade)
	session.merge(new_marks_row)
	if row.H_Int == '-':
		row.H_Int = 0
	if row.H_Ext == '-':
		row.H_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.H_Skip, semester='s4', int_marks=row.H_Int, ext_marks=row.H_Ext, marks=int(row.H_Int)+int(row.H_Ext), status=row.H_Status, grade=row.H_Grade)
	session.merge(new_marks_row)
	if row.SemTotal == '-':
		row.SemTotal = 0
	new_s4_row = S4(regno=row.RegNo,
					# name=row.Name,
					# branch=row.Branch,
					subject1=row.A_Skip,
					subject2=row.B_Skip,
					subject3=row.C_Skip,
					subject4=row.D_Skip,
					subject5=row.E_Skip,
					subject6=row.F_Skip,
					subject7=row.G_Skip,
					subject8=row.H_Skip,
					total=row.SemTotal,
					status=row.SemStatus)
	# new_s4_row.regno = row.RegNo
	session.merge(new_s4_row)
	session.commit()
	print('Importing S4... {}%'.format(int((index/no_of_records)*100)))
print('Importing S4... Done.')

for index, row in df_s5.iterrows():
	if row.A_Int == '-':
		row.A_Int = 0
	if row.A_Ext == '-':
		row.A_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.A_Skip, semester='s5', int_marks=row.A_Int, ext_marks=row.A_Ext, marks=int(row.A_Int)+int(row.A_Ext), status=row.A_Status, grade=row.A_Grade)
	session.merge(new_marks_row)
	if row.B_Int == '-':
		row.B_Int = 0
	if row.B_Ext == '-':
		row.B_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.B_Skip, semester='s5', int_marks=row.B_Int, ext_marks=row.B_Ext, marks=int(row.B_Int)+int(row.B_Ext), status=row.B_Status, grade=row.B_Grade)
	session.merge(new_marks_row)
	if row.C_Int == '-':
		row.C_Int = 0
	if row.C_Ext == '-':
		row.C_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.C_Skip, semester='s5', int_marks=row.C_Int, ext_marks=row.C_Ext, marks=int(row.C_Int)+int(row.C_Ext), status=row.C_Status, grade=row.C_Grade)
	session.merge(new_marks_row)
	if row.D_Int == '-':
		row.D_Int = 0
	if row.D_Ext == '-':
		row.D_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.D_Skip, semester='s5', int_marks=row.D_Int, ext_marks=row.D_Ext, marks=int(row.D_Int)+int(row.D_Ext), status=row.D_Status, grade=row.D_Grade)
	session.merge(new_marks_row)
	if row.E_Int == '-':
		row.E_Int = 0
	if row.E_Ext == '-':
		row.E_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.E_Skip, semester='s5', int_marks=row.E_Int, ext_marks=row.E_Ext, marks=int(row.E_Int)+int(row.E_Ext), status=row.E_Status, grade=row.E_Grade)
	session.merge(new_marks_row)
	if row.F_Int == '-':
		row.F_Int = 0
	if row.F_Ext == '-':
		row.F_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.F_Skip, semester='s5', int_marks=row.F_Int, ext_marks=row.F_Ext, marks=int(row.F_Int)+int(row.F_Ext), status=row.F_Status, grade=row.F_Grade)
	session.merge(new_marks_row)
	if row.G_Int == '-':
		row.G_Int = 0
	if row.G_Ext == '-':
		row.G_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.G_Skip, semester='s5', int_marks=row.G_Int, ext_marks=row.G_Ext, marks=int(row.G_Int)+int(row.G_Ext), status=row.G_Status, grade=row.G_Grade)
	session.merge(new_marks_row)
	if row.H_Int == '-':
		row.H_Int = 0
	if row.H_Ext == '-':
		row.H_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.H_Skip, semester='s5', int_marks=row.H_Int, ext_marks=row.H_Ext, marks=int(row.H_Int)+int(row.H_Ext), status=row.H_Status, grade=row.H_Grade)
	session.merge(new_marks_row)
	if row.SemTotal == '-':
		row.SemTotal = 0
	new_s5_row = S5(regno=row.RegNo,
					# name=row.Name,
					# branch=row.Branch,
					subject1=row.A_Skip,
					subject2=row.B_Skip,
					subject3=row.C_Skip,
					subject4=row.D_Skip,
					subject5=row.E_Skip,
					subject6=row.F_Skip,
					subject7=row.G_Skip,
					subject8=row.H_Skip,
					total=row.SemTotal,
					status=row.SemStatus)
	# new_s5_row.regno = row.RegNo
	session.merge(new_s5_row)
	session.commit()
	print('Importing S5... {}%'.format(int((index/no_of_records)*100)))
print('Importing S5... Done.')

for index, row in df_s6.iterrows():
	if row.A_Int == '-':
		row.A_Int = 0
	if row.A_Ext == '-':
		row.A_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.A_Skip, semester='s6', int_marks=row.A_Int, ext_marks=row.A_Ext, marks=int(row.A_Int)+int(row.A_Ext), status=row.A_Status, grade=row.A_Grade)
	session.merge(new_marks_row)
	if row.B_Int == '-':
		row.B_Int = 0
	if row.B_Ext == '-':
		row.B_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.B_Skip, semester='s6', int_marks=row.B_Int, ext_marks=row.B_Ext, marks=int(row.B_Int)+int(row.B_Ext), status=row.B_Status, grade=row.B_Grade)
	session.merge(new_marks_row)
	if row.C_Int == '-':
		row.C_Int = 0
	if row.C_Ext == '-':
		row.C_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.C_Skip, semester='s6', int_marks=row.C_Int, ext_marks=row.C_Ext, marks=int(row.C_Int)+int(row.C_Ext), status=row.C_Status, grade=row.C_Grade)
	session.merge(new_marks_row)
	if row.D_Int == '-':
		row.D_Int = 0
	if row.D_Ext == '-':
		row.D_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.D_Skip, semester='s6', int_marks=row.D_Int, ext_marks=row.D_Ext, marks=int(row.D_Int)+int(row.D_Ext), status=row.D_Status, grade=row.D_Grade)
	session.merge(new_marks_row)
	if row.E_Int == '-':
		row.E_Int = 0
	if row.E_Ext == '-':
		row.E_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.E_Skip, semester='s6', int_marks=row.E_Int, ext_marks=row.E_Ext, marks=int(row.E_Int)+int(row.E_Ext), status=row.E_Status, grade=row.E_Grade)
	session.merge(new_marks_row)
	if row.F_Int == '-':
		row.F_Int = 0
	if row.F_Ext == '-':
		row.F_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.F_Skip, semester='s6', int_marks=row.F_Int, ext_marks=row.F_Ext, marks=int(row.F_Int)+int(row.F_Ext), status=row.F_Status, grade=row.F_Grade)
	session.merge(new_marks_row)
	if row.G_Int == '-':
		row.G_Int = 0
	if row.G_Ext == '-':
		row.G_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.G_Skip, semester='s6', int_marks=row.G_Int, ext_marks=row.G_Ext, marks=int(row.G_Int)+int(row.G_Ext), status=row.G_Status, grade=row.G_Grade)
	session.merge(new_marks_row)
	if row.H_Int == '-':
		row.H_Int = 0
	if row.H_Ext == '-':
		row.H_Ext = 0
	new_marks_row = Marks(regno=row.RegNo, subject=row.H_Skip, semester='s6', int_marks=row.H_Int, ext_marks=row.H_Ext, marks=int(row.H_Int)+int(row.H_Ext), status=row.H_Status, grade=row.H_Grade)
	session.merge(new_marks_row)
	if row.SemTotal == '-':
		row.SemTotal = 0
	new_s6_row = S6(regno=row.RegNo,
					# name=row.Name,
					# branch=row.Branch,
					subject1=row.A_Skip,
					subject2=row.B_Skip,
					subject3=row.C_Skip,
					subject4=row.D_Skip,
					subject5=row.E_Skip,
					subject6=row.F_Skip,
					subject7=row.G_Skip,
					subject8=row.H_Skip,
					total=row.SemTotal,
					status=row.SemStatus)
	# new_s6_row.regno = row.RegNo
	session.merge(new_s6_row)
	session.commit()
	print('Importing S6... {}%'.format(int((index/no_of_records)*100)))
print('Importing S6... Done.')