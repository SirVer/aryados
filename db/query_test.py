from sqlalchemy import Column, Integer, Float, Date, String, VARCHAR
from sqlalchemy import ForeignKey
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Marks(Base):
	"""docstring for Marks"""
	__tablename__ = 'marks'
	regno = Column(VARCHAR, primary_key=True, nullable=False)
	subject = Column(VARCHAR, primary_key=True, nullable=False)
	int_marks = Column(Integer)
	ext_marks = Column(Integer)
	status = Column(VARCHAR)
	grade = Column(VARCHAR)

class Student(object):
	"""Abstract class for student"""
	@declared_attr
	def __tablename__(cls):
		return cls.__name__.lower()
	regno = Column(VARCHAR, ForeignKey('marks.regno'), primary_key=True, nullable=False)
	name = Column(VARCHAR)
	branch = Column(VARCHAR)

class S1S2(Base, Student):
	"""Class for S1S2 table"""
	__tablename__ = 's1s2'
	regno = Column(VARCHAR, ForeignKey('marks.regno'), primary_key=True, nullable=False)
	subject1 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject2 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject3 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject4 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject5 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject6 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject7 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject7 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject8 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject9 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject10 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject11 = Column(VARCHAR, ForeignKey('marks.subject'))
	total = Column(Integer)
	status = Column(VARCHAR)

class S3(Base, Student):
	"""Class for S3 table"""
	__tablename__ = 's3'
	regno = Column(VARCHAR, ForeignKey('marks.regno'), primary_key=True, nullable=False)
	subject1 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject2 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject3 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject4 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject5 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject6 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject7 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject7 = Column(VARCHAR, ForeignKey('marks.subject'))
	subject8 = Column(VARCHAR, ForeignKey('marks.subject'))
	total = Column(Integer)
	status = Column(VARCHAR)

engine = create_engine('sqlite:///rsms.db')
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()
# for row in session.query(S3).filter_by(regno='14012561'):
# 	for field in row:
# 		print(field)
for m in session.query(S3).all():
	for x in S3.__table__.columns:
	    print(getattr(m, x.__str__().split('.')[1]))