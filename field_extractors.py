import spacy 
import sys

from misc import is_substring
from aryados import map_phrase
from db.tables import OperationMap, FieldMap

def is_name(phrase):
	from sqlalchemy.ext.declarative import declarative_base
	from sqlalchemy import create_engine
	from sqlalchemy.orm import sessionmaker
	from db.tables import db_url, Base, Student
	
	engine = create_engine(db_url)
	Base.metadata.create_all(engine)
	DBSession = sessionmaker(bind=engine)
	session = DBSession()

	for record in session.query(Student):
		if is_substring(str(phrase), record.name):
			return record.name.title()
	return False

def get_split_token(doc):
	no_of_verbs = 0
	for token in doc:
		if token.pos_=='ADP':
			return token
	else:
		for token in doc:
			if token.pos_=='VERB':
				return token

def get_operation(doc):
	if map_phrase(doc[0], OperationMap):
		if map_phrase(doc[0], OperationMap).operation == 'select':
			split = get_split_token(doc)
			for token in doc:
				if token == split:
					break
				if map_phrase(token, OperationMap) and map_phrase(token, OperationMap).aggregate:
					return map_phrase(token, OperationMap).operation
		return 'select'
	return None

def get_requested_fields(doc):
	fields = []
	split_token = get_split_token(doc)
	# if split_token.pos_ == 'ADP':
	for token in doc:
		if token == split_token:
			break
		if token.pos_ == "NOUN":
			for np in doc.noun_chunks:
				if np.root == token:
					fields.append(np)
		if len(fields) > 0 and token.dep_ == "conj" and token.head == fields[0].root:
			for np in doc.noun_chunks:
				if np.root == token:
					fields.append(np)
	# elif split_token.pos_ == 'VERB':
	# 	for token in doc:
	# 		if token == split_token:
	# 			break
	# 		if token.dep_=="dobj" or token.dep_=="nsubj":
	# 			for np in doc.noun_chunks:
	# 				if np.root == token:
	# 					fields.append(np)
	# 		if len(fields) > 0 and token.dep_=="conj" and token.head==fields[0].root:
	# 			for np in doc.noun_chunks:
	# 				if np.root == token:
	# 					fields.append(np)
	return fields

def get_clauses(doc):
	split_token = get_split_token(doc)
	split_index = 0
	for token in doc:
		if token == split_token:
			break
		split_index+=1
	clauses_span = doc[split_index+1:]
	nouns = []
	noun_phrases = []
	proper_nouns = []
	names = []
	nums = []
	table = None
	clauses = []
	clause_start_index = split_index + 1
	clause_end_index = clause_start_index
	for token in clauses_span:
		if token.pos_ == 'NOUN':
			# Adds the token to the nouns list
			nouns.append(token)
			# Adds the noun phrase that contains the token to the noun_phrases list 
			for np in doc.noun_chunks:
				if np.root == token:
					noun_phrases.append(np)
		if token.pos_ == 'PROPN':
			# Checks if token is a name, adds the corresponding noun phrase to the
			# names list (if not already present)
			for np in doc.noun_chunks:
				if token in np:
					name = is_name(np)
					if name and name not in names:
						names.append(name)
						break
					elif name in names:
						break
					for token2 in np:
						name = is_name(token2)
						if name and name not in names:
							names.append(name)
							break
			# Adds the token to the proper_nouns list if not a name
			# For Shenela's sake: the else is part of the for
			else:
				proper_nouns.append(token)
		if token.pos_ == 'NUM':
			nums.append(token)
		if map_phrase(token, FieldMap) and map_phrase(token, FieldMap).field_type == 'table':
			table = map_phrase(token, FieldMap).field
		if token.pos_ == 'CCONJ' or token == doc[-1]:
			clause = {
					'string': doc[clause_start_index:clause_end_index+1],
					'nouns': nouns,
					'noun_phrases': noun_phrases,
					'proper_nouns': proper_nouns,
					'names': names,
					'nums': nums,
					'table': table,
					}
			clauses.append(clause)
			clause_start_index = clause_end_index
			nouns = []
			noun_phrases = []
			proper_nouns = []
			names = []
			nums = []
		clause_end_index+=1
	return clauses

def main():
	nlp = spacy.load('en_core_web_sm')
	doc = nlp(sys.argv[1])
	print(get_split_token(doc))
	print(get_operation(doc))
	print('Requested fields:', get_requested_fields(doc))
	print('Clauses:', get_clauses(doc))

if __name__ == '__main__':
	main()