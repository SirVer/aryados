#!/usr/bin/env python3

import speech_recognition as sr

r = sr.Recognizer()

with sr.Microphone() as source:
	print('Say something:')
	audio = r.listen(source)
	print('Recorded.')

username = "f3110e23-41ae-4277-bffd-f4111197620c"
password = "gXW2yiCidV7L"

try:
		print("Simple output: ", r.recognize_ibm(audio, username, password))
except sr.UnknownValueError:
		print("Sphinx could not understand audio")

try:
		print("Full output: ", r.recognize_ibm(audio, username, password, max_alternatives=3, show_all=True))
except sr.UnknownValueError:
		print("Sphinx could not understand audio")
