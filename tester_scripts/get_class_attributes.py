#!/usr/bin/env python3

import pocketsphinx as p

att_dict = p.Decoder.seg.__dict__

att_doc = {}
doc = ''
for att in att_dict.keys():
	if att_dict[att].__doc__ is None:
		continue
	print(att + '\n' + att_dict[att].__doc__ + '\n\n')