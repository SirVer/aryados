import spacy 
import sys

nlp = spacy.load('en_core_web_sm')
	#i=1

# string = str(string)
string = u'List the TOC marks and aggregates of the students where the semester is S7'
doc = nlp(string)

for token in doc:
	print (token,'\t',token.pos,'\t',token.pos_,'\t')
print(" ")

for token in doc.noun_chunks:
	print(token.text,'\t',token.root.text,'\t',token.root.dep_,'\t',token.root.head.text,'\t',token.root.head.pos_,'\t',[child for child in token.root.children],'\t')
print(" ")

for np in doc.noun_chunks:
	if np.root.dep_=="dobj":
		print(np.text)
		for np2 in doc.noun_chunks:
			# print(np2.root.text, np2.root.head.text, np2.root.dep_)
			if np2.root.dep_=="conj" and np2.root.head==np.root:
				print(np2.text)
				break