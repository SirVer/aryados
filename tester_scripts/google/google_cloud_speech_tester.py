#!/usr/bin/env python3

import speech_recognition as sr

r = sr.Recognizer()

with sr.Microphone() as source:
	print('Say something:')
	audio = r.listen(source)
	print('Recorded.')

GOOGLE_CLOUD_SPEECH_CREDENTIALS = r"""{
	"type": "service_account",
	"project_id": "spartan-cedar-175516",
	"private_key_id": "ec4974afa3425d847b8d8f98e58f9cb49286880b",
	"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC0o6cMxK+ux3Pb\nLaS79g+8idmCYGh3LIlMwp2uHWergnwGH5vn39CmIIr0t4hLUmDANApa5MhZ+JfS\n/8FQVdOGx8O8LB36WfhElSBGwCoUH1ykya0TmKRVegEfBMwonZGa+nQS4fhsh0gF\nLorwwWXsHh3+0NllVPmi+aGYrxWWMFG0l8IFcsqriUhVCBPXWyk9wmge1FXQjsSQ\n+0xsoLRcLGYpwg3l7B5b2+KbiS8Lunf+Qouq8RUwGFoGwO263xL1s4e3skROs5Iy\nKI+5c9wEOtBbfw0T+AyUNGKBv0cugZDguVKXC/Zk4+SNsvEx23UsIJ3qbb9f2cdy\nW6xAjmbDAgMBAAECggEACWmC+o2x5yqw51ulRD7OU9E7Pul9ykK6vP/z1zN245qq\nbdVAg5cZ85FyqcznMlc8SOU6/JwoscJJlS64etJ4TQMI72BPrGv6+Lz+dJy3Ykjj\nMdDpMx2NVVsf/WbXPCWtqejzUYBFuD3xIjyOz8UOQUXR45MHjWLmmQD4r6LAYvqJ\nkcjtuNkUzbP77oHBdxU0wrR0iwCKFq6YFw/MH8CRQjQamhkEeyLeM4Oo0/YLClBH\nXrccvJsJ1z0tvjHzL8c+V9Kryv4t7fFkyWAm3aYL1l4FGL0qYJaa01/ZkHNrKNp8\nOROPoDiwGx30lH2vNqagi2M79jqDOxqKtigsdVkFoQKBgQDX51qkwg8poVCuiZ0n\nLjBInpLDWVfLm+1RfFw1Bg+D7eRLJq5PbhCggIh9gUMRh1dWagv1Gesu7M2cQErf\nzDrvA+ulB457IABhnm5TO/3qLhKoiDOh1iT5N9sPRHRZRS/64Xa76F7OKR4Bageu\npPxtrZQPCCljbhxSj3yNIOnKoQKBgQDWL7sbv52H0u/1EEcj1ra9/kRX0IRFVg2A\nQgXoLYCHhf4a7AP2cnc1C3Xaxwav1AA36h9mWzv1+YLclJ1EKOxjzTS9v6Tp3Vyz\nwwKcUHROoHPyF2DMKrxbJO3R9K/3pGgj5tjwIrV2/Gz7U2+bheJkv4lFVQzq/aO5\naIFlv0964wKBgFtmNZlKDeExyVYaK81KYGZduwO37tLjT7QPUXmg9zCCpQKrfV9i\n2OcZgq6e6iWhhfevLSC1uDpFRUXAcfIMI2bL4hYa/NZi1cZ9q3bRDsz0dDGkdzcr\n0LswSixMbnTcNVOf3wPtezULcwhmOppUfghwUx2J9vuAwbyxwah9JnwBAoGBAMlW\n2QZ+m3DgYtz1TkpsPiBfcpMx2gTrnYlhhvf4PggndvSLxDXY6CIfAcyEps6F9Saa\nmKW5QaY5xD/iG+vEpGI0tk1gZ3/HrKr2t++E2bBHuHZsb3u8+I6dKknhP8EE9Tg6\netdRaU/OfgvLJPEHbAfU3Q8loh0MDOnvSSQCMI6NAoGAIu4IrKcScXgbljAFDy2s\nacXYtA6CVMLlGfjaM9OCKFWxP5xbeq1v4oDq+BGNAbtw7kdjj+IpkEED4+1G47MC\nacH8PmfusgRvt54TnWpYH+3L2lwD+9lsrsceUmiZiaJbgy8caZtCkcDM8or8OAQk\nChQtDGyW6+CyZREnVK4jltw=\n-----END PRIVATE KEY-----\n",
	"client_email": "aryados-google-speech@spartan-cedar-175516.iam.gserviceaccount.com",
	"client_id": "103199247513493153606",
	"auth_uri": "https://accounts.google.com/o/oauth2/auth",
	"token_uri": "https://accounts.google.com/o/oauth2/token",
	"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
	"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/aryados-google-speech%40spartan-cedar-175516.iam.gserviceaccount.com"
}"""

try:
		print("Simple output: ", r.recognize_google_cloud(audio, credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, show_all = False))
except sr.UnknownValueError:
		print("Google Cloud Speech could not understand audio")
except sr.RequestError as e:
		print("Could not request results from Google Cloud Speech service; {0}".format(e))

try:
		print("Full output: ", r.recognize_google_cloud(audio, credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, max_alternatives=3, show_all = True))
except sr.UnknownValueError:
		print("Google Cloud Speech could not understand audio")
except sr.RequestError as e:
		print("Could not request results from Google Cloud Speech service; {0}".format(e))
