#!/usr/bin/env python3

import speech_recognition as sr
from shutil import copyfile
from os import rename

sr_path = sr.__file__
rename(sr_path, sr_path+'.bak')
copyfile('google_patched__init__.py', sr_path)