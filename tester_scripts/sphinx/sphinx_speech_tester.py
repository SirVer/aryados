#!/usr/bin/env python3

import speech_recognition as sr

r = sr.Recognizer()

with sr.Microphone() as source:
	print('Say something:')
	audio = r.listen(source)
	print('Recorded.')

try:
		print("Simple output: ", r.recognize_sphinx(audio))
except sr.UnknownValueError:
		print("Sphinx could not understand audio")
