import spacy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker

import sys
import pprint

import stt
import field_extractors as ex
from db.tables import db_url, Base, Marks, Student, S1S2, S3, S4, S5, S6, OperationMap, RelationshipMap, FieldMap, FieldTableMap
# from db.get_records import get_records
from misc import is_substring, merge_dicts

# Creates the connection to the database
engine = create_engine(db_url)
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

db_tables = [Marks, Student, S1S2, S3, S4, S5, S6]

def map_phrase(phrase, table=None):
	'''
	If table is None, checks each implementation table for a mapping for the given phrase
	Else, checks specified table for the mapping
	'''
	if not table:
		impl_tables = [OperationMap, RelationshipMap, FieldMap]
	else:
		impl_tables = [table]
	for impl_table in impl_tables:
		for row in session.query(impl_table):
			x = row.field if impl_table is FieldTableMap else row.phrase
			if is_substring(str(phrase), x):
				return row
		else:
			for row in session.query(impl_table):
				x = row.field if impl_table is FieldTableMap else row.phrase
				if isinstance(phrase, spacy.tokens.span.Span) and is_substring(str(phrase.root), x):
					return row
	return None

def match_field_and_value(clause):
	noun_mappings = []
	unmapped = []
	conditions = []
	for np in clause['noun_phrases']:
		if map_phrase(np, FieldMap):
			noun_mappings.append(map_phrase(np))
		else:
			unmapped.append(np)
	for pn in clause['proper_nouns']:
		if map_phrase(pn, FieldMap):
			noun_mappings.append(map_phrase(pn))
		else:
			unmapped.append(pn)
	print([(mapping.phrase, mapping.field, mapping.field_type) for mapping in noun_mappings])	# Debug
	for mapping in noun_mappings:
		type_pairs = {'marks': 'subject', 'int_marks': 'subject', 'ext_marks': 'subject', 'branch': 'branch', 'status': 'status'}
		if mapping.field in type_pairs:
			for mapping2 in noun_mappings:
				if mapping2.field_type == type_pairs[mapping.field]:
					conditions.append({'field': type_pairs[mapping.field], 'relationship': '=', 'value': mapping2.field})
					if clause['table'] and conditions[-1]['field'] in get_table_from_name(clause['table']).__dict__:
						conditions[-1]['table'] = clause['table']
					noun_mappings.remove(mapping2)
					break
		# if mapping.field == 'marks' or mapping.field == 'int_marks' or mapping.field == 'ext_marks':
		# 	for mapping2 in noun_mappings:
		# 		if mapping2.field_type == 'subject':
		# 			conditions.append({'field': 'subject', 'relationship': '=', 'value': mapping2.field})
		# 			if clause['table'] and conditions[-1]['field'] in get_table_from_name(clause['table']).__dict__:
		# 				conditions[-1]['table'] = clause['table']
		# 			noun_mappings.remove(mapping2)
		# 			break
		# elif mapping.field == 'branch':
		# 	for mapping2 in noun_mappings:
		# 		if mapping2.field_type == 'branch':
		# 			conditions.append({'field': 'branch', 'relationship': '=', 'value': mapping2.field})
		# 			if clause['table'] and conditions[-1]['field'] in get_table_from_name(clause['table']).__dict__:
		# 				conditions[-1]['table'] = clause['table']
		# 			noun_mappings.remove(mapping2)
		# 			break
		# elif mapping.field == 'status':
		# 	for mapping2 in noun_mappings:
		# 		if mapping2.field_type == 'status':
		# 			conditions.append({'field': 'status', 'relationship': '=', 'value': mapping2.field})
		# 			if clause['table'] and conditions[-1]['field'] in get_table_from_name(clause['table']).__dict__:
		# 				conditions[-1]['table'] = clause['table']
		# 			noun_mappings.remove(mapping2)
		# 			break
	for name in clause['names']:
		conditions.append({'field': 'name', 'relationship': '=', 'value': str(name)})
	for num in clause['nums']:
		for mapping in noun_mappings:
			if mapping.field_type == 'num':
				conditions.append({'field': mapping.field, 'value': int(str(num))})
				noun_mappings.remove(mapping)
				for token in clause['string']:
					if map_phrase(token, RelationshipMap):
						conditions[-1]['relationship'] = map_phrase(token, RelationshipMap).relationship
						break
				else:
					conditions[-1]['relationship'] = '='
				if clause['table'] and conditions[-1]['field'] in get_table_from_name(clause['table']).__dict__:
					conditions[-1]['table'] = clause['table']
				break
	return conditions

def get_table_from_name(tablename):
	for table in db_tables:
		if table.__tablename__ == tablename:
			return table

def get_records(req_fields, conditions, additional_tables=None):
	table_sets = []
	req_fields = list(set(req_fields))
	print('req:', req_fields)
	for field in req_fields: 
		if map_phrase(field, FieldTableMap):
			possible_tables = map_phrase(field, FieldTableMap).tables.split(',')
			print('possible_tables:', possible_tables)
			if len(possible_tables) > 1:
				for condition in conditions:
					if 'table' in condition and condition['table'] in possible_tables:
						table_sets.append(set([condition['table']]))
					elif additional_tables:
						table_sets.append(set(additional_tables))
			else:
				table_sets.append(set(possible_tables))
	print('sets:', table_sets)
	if req_fields:
		table_set = set.intersection(*table_sets) #to remove repetitive table names in the list because a list can have repeated values
		for ts in table_sets:
			if not set.intersection(table_set,ts):
				table_set.add(list(ts)[0])
		table_list = [get_table_from_name(table) for table in sorted(table_set, reverse=True)]
	else:
		table_list = [Student]
	# if additional_tables:
	# 	table_list.extend(additional_tables)
	print(table_list)
	for condition in conditions:
		if 'table' in condition and condition['table']:
			condition['table'] = get_table_from_name(condition['table'])
		elif map_phrase(condition['field'], FieldTableMap):
			condition['table'] = get_table_from_name(map_phrase(condition['field'], FieldTableMap).tables.split(',')[0])
	print(conditions)

	filters = []
	for condition in conditions:
		if condition['relationship'] == '=':
			if type(condition['value']) is str:
				filters.append(getattr(condition['table'], condition['field']).like('%'+condition['value']+'%'))
			else:
				filters.append(getattr(condition['table'], condition['field']) == condition['value'])
		elif condition['relationship'] == '!=':
			filters.append(~getattr(condition['table'], condition['field']).like(condition['value']))
		elif condition['relationship'] == '<':
			filters.append(getattr(condition['table'], condition['field']) < condition['value'])
		elif condition['relationship'] == '>':
			filters.append(getattr(condition['table'], condition['field']) > condition['value'])
		elif condition['relationship'] == '<=':
			filters.append(getattr(condition['table'], condition['field']) <= condition['value'])
		elif condition['relationship'] == '>=':
			filters.append(getattr(condition['table'], condition['field']) >= condition['value'])

	rows = []
	# query = session.query(*table_list).join(*set([condition['table'] for condition in conditions]).difference(table_list))
	query = session.query(*table_list)
	for table in set([condition['table'] for condition in conditions]):
		query = query.filter(table.regno == table_list[0].regno)
		# query = query.join(table)
	query = query.filter(*filters)
	print('Query:', query)
	if len(table_list) == 1:
		for row in query:
			row = inspect(row).dict
			del row['_sa_instance_state']
			rows.append(row)
	else:
		for row_tuple in query:
			combined_row = merge_dicts([inspect(row).dict for row in row_tuple])
			del combined_row['_sa_instance_state']
			rows.append(combined_row)

	if req_fields:
		new_rows = []
		for row in rows:
			new_row = {}
			for field in req_fields:
				if field in row:
					new_row[field] = row[field]
			new_rows.append(new_row)
		return new_rows
	else:
		return rows

def main():
	nlp = spacy.load('en_core_web_sm')
	if len(sys.argv) == 1:
		transcript = stt.speech_to_text(preferred_phrases=stt.preferred_phrases, language="en-IN")
		print(transcript)
		doc = nlp(transcript)
	elif len(sys.argv) == 2:
		doc = nlp(sys.argv[1])
	else:
		print('ERROR: Too many arguments')
		return False
	print(ex.get_operation(doc))
	req_fields = ex.get_requested_fields(doc)
	print(req_fields)
	rq_fields = []
	for rq in req_fields:
		if not map_phrase(rq,OperationMap):
			rq_fields.append(rq)
	print(rq_fields)
	req_fields = rq_fields
	req_fields = [map_phrase(field, FieldMap).field for field in req_fields]
	if '*' in req_fields:
		req_fields.remove('*')
	clauses = ex.get_clauses(doc)
	tables = []
	for clause in clauses:
		if clause['table']:
			tables.append(clause['table'])
	operation = ex.get_operation(doc)
	print('Requested fields:', req_fields)
	print('Clauses:', clauses)
	print('Operation:', operation)
	conditions = []
	for clause in clauses:
		conditions.extend(match_field_and_value(clause))
	print('Conditions:', conditions)
	records = get_records(req_fields, conditions, tables)
	if operation == "count":
		print('No. of results:', len(records))
	elif operation == "avg":
		avg=0
		for record in records:
			for key in record:
				if map_phrase(key,FieldMap).field_type=='num':
					avg=avg+record[key]
		avg=avg/len(records)
		print(avg,"is the average")
	elif operation == 'min':
		num_col = None
		for record in records:
			for key in record:
				if map_phrase(key,FieldMap).field_type=='num':
					num_col = key
		print('Minimum : ',sorted(records, key = lambda record: record[num_col])[2])
	elif operation == 'max':
		num_col = None
		for record in records:
			for key in record:
				if map_phrase(key,FieldMap).field_type=='num':
					num_col = key
		print('Maximum : ',sorted(records, key = lambda record: record[num_col],reverse = True)[0])

	else:	
		input('See result? ')
		print('\n=========== RESULT ===========\n')
		for record in records:
			pprint.pprint(record)
		print('No. of results:', len(records))

if __name__ == '__main__':
	main()