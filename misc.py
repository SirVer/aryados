import re

def is_substring(substring, string):
	return re.compile(r'\b({0})\b'.format(substring), flags=re.IGNORECASE).search(string)

def merge_dicts(dicts):
	new_dict = {}
	for d in dicts:
		new_dict.update(d)
	return new_dict